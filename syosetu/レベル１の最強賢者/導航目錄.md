# CONTENTS

レベル１の最強賢者  
レベル１の最強賢者 ～ 呪いで最下級魔法しか使えないけど、神の勘違いで無限の魔力を手に入れて最強に！ ～  
等級１的最強賢者  
等級１的最強賢者　因為詛咒只能使用最下級魔法，但是因為神的失誤獲得無限魔力所以成為最強！  

作者： 木塚 麻弥  



- :closed_book: [README.md](README.md) - 簡介與其他資料
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E3%83%AC%E3%83%99%E3%83%AB%EF%BC%91%E3%81%AE%E6%9C%80%E5%BC%B7%E8%B3%A2%E8%80%85.ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/blob/master/lib/locales/)
-  :heart: [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/%E7%AD%89%E7%B4%9A%EF%BC%91%E7%9A%84%E6%9C%80%E5%BC%B7%E8%B3%A2%E8%80%85.epub) :heart:  ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/out/%E7%AD%89%E7%B4%9A%EF%BC%91%E7%9A%84%E6%9C%80%E5%BC%B7%E8%B3%A2%E8%80%85.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/)
- :mega: [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitlab.com/novel-group/txt-source/blob/master/syosetu/レベル１の最強賢者/導航目錄.md "導航目錄")




## [序章](00000_%E5%BA%8F%E7%AB%A0)

- [邪神の暗躍](00000_%E5%BA%8F%E7%AB%A0/00010_%E9%82%AA%E7%A5%9E%E3%81%AE%E6%9A%97%E8%BA%8D.txt)


## [第一章 新たな始まり](00010_%E7%AC%AC%E4%B8%80%E7%AB%A0%20%E6%96%B0%E3%81%9F%E3%81%AA%E5%A7%8B%E3%81%BE%E3%82%8A)

- [転生](00010_%E7%AC%AC%E4%B8%80%E7%AB%A0%20%E6%96%B0%E3%81%9F%E3%81%AA%E5%A7%8B%E3%81%BE%E3%82%8A/00010_%E8%BB%A2%E7%94%9F.txt)
- [狀態欄確認](00010_%E7%AC%AC%E4%B8%80%E7%AB%A0%20%E6%96%B0%E3%81%9F%E3%81%AA%E5%A7%8B%E3%81%BE%E3%82%8A/00020_%E7%8B%80%E6%85%8B%E6%AC%84%E7%A2%BA%E8%AA%8D.txt)
- [魔法訓練](00010_%E7%AC%AC%E4%B8%80%E7%AB%A0%20%E6%96%B0%E3%81%9F%E3%81%AA%E5%A7%8B%E3%81%BE%E3%82%8A/00030_%E9%AD%94%E6%B3%95%E8%A8%93%E7%B7%B4.txt)
- [無限の魔力](00010_%E7%AC%AC%E4%B8%80%E7%AB%A0%20%E6%96%B0%E3%81%9F%E3%81%AA%E5%A7%8B%E3%81%BE%E3%82%8A/00040_%E7%84%A1%E9%99%90%E3%81%AE%E9%AD%94%E5%8A%9B.txt)

