对於已经习惯了这种练习的我而言,1000次空挥幾乎不会消耗什麼体力。

咕噜噜……

不过肚子却有点饿了呢。

“去吃饭吗？”

“好啊~~”

拉起坐在草坪上有些倦意的查德，我将剑收回鞘中……

诺布尔学院的西南角，是名为赤海葵之间的餐厅。

前幾天一直是到校外买些食物吃的，不过今天有同伴一起的话还是在校内吃好了。

这裡由於是骑士学院，为了培养骑士的忍耐品质，所以饭菜好像非常难吃。

我推开有着两位智天使双翼相向浮雕的檀木大门。

主题为白色非常有清洁之感的典雅大房间，就连桌椅也是白色成套的。

骑士与准骑士的学生们放鬆下来闲谈着，偶尔也有一两声较大的声音从人群中传出。

还算是非常有活力的景象吧……

不过大家在将视线转到推开门的我身上时。

这些热闹的声音戛然而止。

“这裡难道有什麼特别的规矩吗？”

一边顺手带上後边的门，一边低声问着身旁的查德。

“……不是啦，是因为你啦！是你啦！”

虽然为了不影响他人，查德也凑到了耳边，但总感觉语气有些莫名其妙的激动。

“我？”

“所以之前就说过了吧！希尔维娅公主的事情让你很出名了啊……”

“可是之前根本就没来过这个餐厅来着……”

“你自己看看那个吧……”

他指着贴在墙上的某张纸。

我靠近了墙壁……

哈？！这是什麼啊……

【不要脸贴在公主身边的害虫……艾伦·克鲁·莎法尔……】

红得就像用血写的大字，然後画了一张非常扭曲的画像。

“只有姓写对了啊。”

而且为什麼这种画像能让人留意啊……明明完完全全不一样的说。

“……我觉得最大的原因应该是……艾伦带着剑……”

“是艾文啊！！”

“对不起！对不起！！”

莱纳森连忙弯下身不停道着歉。

是吗？其实这时候出门到市内去解决午餐也没什麼。

不过这样就是认输了吧，我又没做错什麼虽然有些讨厌，毕竟是今後要一直生活的学院，可以的话还是让大家习惯一下吧。我也没什麼敌意，只要将这样的想法灌输给大家的话……

“我们到裡面一点的位置坐下吧。”

“喂，等下啦。”

没有等他们，我直接找了一个拐角的位置坐了下来。

将剑放下，点餐的女仆也急忙迎了上来。

“中午好，艾文先生~~想要吃点什麼呢？”

“诶？你也知道我的名字吗？”

女孩子摆了摆长长的单马尾，为了掩饰笑意遮住了自己的嘴唇。

感觉很像克莉斯呢。

“呼呼，现在大家对艾文先生应该是无人不知吧。”

又来了吗？

“……我是不太清楚这位希尔维娅公主到底让大家注目到什麼程度，但是我真的是什麼都不知道就被她抱住了……”

“嘛~~但是大家不是这麼想的。失礼了，也许您本身并不是很吸引大家的目光，但是那位希尔维娅公主可以会将靠近自己5米的人随意变身动物的残暴姬哟，就连守护骑士们都跟她保持着距离，所以您的事迹也算是破天荒啦……对了，这句话千万别告诉任何人哦，人家也不想被变成蛞蝓啊。”

“蛞蝓？！那位公主对女孩子也做这样的事情吗？”

“确实是有啦……嘛~~想要继续聊天的话先点些什麼如何？对了，我叫做米撒拉，常来光顾哦……艾文~~还有下次可别带剑进来了哟，有些前辈们看上去意见比较大呢。”

她可爱地眨了眨眼睛，竖起一根手指指着天花板，视线却透过遮挡脸颊的手臂看向右边。

唔……

幾位高大的男生，穿着准骑士的战袍，已经进入实战课程的学长们啊。

他们的眼神确实非常凶恶，随时冲过来也不奇怪。

“那麼，这个蜥蜴浓汤吧。对了……查德你们要吃什麼吗？”

“那个吗？超级难吃啊！！”

就仿佛闻到了菜的味道，查德捏着鼻子一副厌恶的神情。

“诶？这样啊……以前还很喜欢这道菜呢。”

“这裡可不是你的宅子啊，大少爷……”

“啰……啰嗦！”

不知不觉又回到小时候那种任性的状态了吗，不行不行……但是这样说多少有些不甘心，明明你们也是贵族来着。

“我就要这个了。”

“你非要这麼固执我也不说什麼了，那麼我要罗宋汤。”

“烤火鸟翅膀。”

“……蔬菜沙拉”

“知道了哟~~请各位稍稍等一下~~”

踩着轻快的脚步，米撒拉再次一眨眼消失在学生当中了。

“好可爱啊……米撒拉小姐……”

“什麼啊？对她感兴趣吗？莱纳森小弟~~”

“不……只是……”

容易害羞的莱纳森瞬间就像女孩子那样整个脸瞬间变成了红色。

“没什麼啦。要敢於面对自己的心情哦，谁让她这麼可爱来着不是吗？”

“……嗯”

“不过还真是看不出来，莱纳森真是会很有看女孩子眼光的类型呢。”

“欸……不是……不是这样的啊，因为想成为吟游诗人的话……必须要有审视美的眼光才行。”

他慌慌张张地挥舞着手臂，显得有点大的制服穿在莱纳森身上有些松松垮垮的。

然而这种时候，他的口袋中的东西便轻易掉了出来。

可是很快就被人拾起来了……

我们一齐看向拿起从莱纳森口袋中掉出笔记本的男生。

“哟~~我来看看……哈哈哈，这还真是绝妙的句子啊。”

“沉浸入三日月之苍的倒影，浮出对不可触及的妳之梦。”

“呼呼呼，小子，你真的是男生吗？这裡的诗一样的玩意该不会是想对米撒拉吧。”

“这麼娘娘腔的玩意……呵呵，我再来看看前面的，嘛~~不如念给大家听听好了。”

是刚才的三个学长，面对他们的戏弄，莱纳森惨白着脸连夺回的勇气也没有，但是眼泪仿佛随时就要从他的大眼睛中夺眶而出。

“停手吧，学长们。”

我站了出来，不管怎麼说也不能看着不管了。

“哈？你小子想管闲事？这可是男人间的事情哦，你也是个男人的话就识相点就别插手！！”

“对着不是战士的人做出这种事的三流骑士也配说这种话吗？”

“你说什麼！？你这黄毛小子。”

三人就像狩猎的狼一样瞬间分了开来，围在了我的身边展开了架势，看起来是认真的。

战鬥之前的杀气皮肤已经感受得相当清楚了。

“喂！学长，你最好停手比较好……准骑士私斗的事情传了出去。”

一直沉默的布朗这时候却非常準确地抓住了他们的弱点想要避免争鬥。

“私斗？只是教训一下谁都不爽的傢伙罢了。这个叫做艾文的小子，仗着欺骗公主的本事，变得有些嚣张啊。”

好像征求认同感一样，他环视着周围的学生。

虽然也有很多人并不想观看这种下品的争鬥离开了餐厅，但是也存在很多想看热闹的学生。

“……艾文……是我不好，不必为……”

还不等莱纳森说完，三人的气息已经散发开了，正好……我也不想听他说完。

一个人已经在身後了，抓起摆在餐厅的扫帚砸了下来。

不愧是实战训练中的准骑士，确实技术和速度不是街头的混混们能比的，即使是扫帚也有利刃一般的气势，或许听从父亲的指示来这裡并没有错吧。

如果你还能再快一点的话！

我集中了精神，判断着攻击的轨道。

45度的斜向斩击。

长剑流的一般技“坠斑鸠”。

对於这种程度的斩击，不需要回头……

我右手抓住预想的轨道，牢牢捏住扫帚。

“唔！！”

用右脚猛踏地面，将左脚尽量向後踢去。

虽然对方的身高要比我高得多，但是踢击距离是拳击的1.5倍。

“啊！”

有中了的实感，踢中对方的下巴了。

椅子被撞散开来的声音响在耳边，但愿你没咬到舌头吧。

解决一个。

看到这幅情景，两人也吃了一惊，但是迅速反应了过来，同时向後退了一步。

然後其中一人立刻又冲了上来。

佯攻吗？

我迅速蹲下将右腿横扫。

这位前辈也瞬间失去了平衡，趁势踩上了他的背部。

这回轮到我进攻了！

完全没有猜到这种连续攻击的方式，带头的那位健壮的准骑士在下意识下用双手护住面部。

没用哦。

瞄准着对方的脖颈，我透过他的手臂，用膝盖窝紧紧卡住，然後双手拽着他拿着笔记本的右手。

我们两个纷纷倒在地上。

“想莱纳森道歉，学长！”

“你这混蛋！唔？！”

他尝试着挣脱，但是完全没有效果。

克莉斯交给我的近身格闘技非常有用……

“这样下去您的手臂会断掉的，虽然有出色的圣职者的话应该很快就能治好吧，但是这种痛苦可不是一般人能承受的哦。”

“混蛋，我也是骑士啊……”

“真是在奇怪的地方有着尊严呢。”

明明欺负着没有力量的人……

那麼，算我失礼了！！

喀哒~~

清脆的声音回响在赤海葵之厅。