




「你们是什麼人。既然能到这个阶层那应该是优秀的冒险者吧，但我对你们没有印象。不记得自己忘了检查啊」

　福奥尔好像说了些什麼，不过无所谓。
　他们是我重要的――

「嗯~。虽然不知道你是谁，但我们可不是冒险者。我们是那边的，土屋的重要――家人啊」

「嗯，是家人哦」

　没错，在祭之岛相遇的没有血缘关系的家人。

「权藏 、萨乌瓦、敏特孔多利雅。为什麼，怎麼到这裡来的……」

「呀，英雄在最关键的时侯登场不是当然的吗」

　穿着没有袖子的衬衫的权藏得意地笑着。啊啊，真怀念啊，那稍微令人感到不爽的脸庞。
　无视他的表情……看起来有经过相当地锻炼啊。
　结实的两腕上隐约残留着无数的伤痕。从分别以後已经过了一年以上了，比起那时更加野性了不少。

「吵死了。是敏特说与哥哥的连接断了，完全被切断了，大危机啊！ 的骚动着，所以就到大陆来了」

　立刻把挺胸自豪的说着的权藏从後脑勺放倒的萨乌瓦莞尔一笑，回答了我的问题。
　萨乌瓦也成长很多了呢。穿着贴身的皮制衣物，体型都能看得出来。
　现在是在成长期吧。身体出现女性特有的纤细腰身，胸部更是已经超越了樱。樱醒过来後会受到打击吧……。
　脸也从少女的脸稍微变得像是大人。就算和权藏站在一起也没有违和感了。

「吓了一大跳！　与敏特孔多利雅的连接突然不见，吓了一跳，赶紧去找他们俩个，向回到岛上的船的船长请求，吓了一跳，追到大陆来了！」

　敏特孔多利雅滔滔不绝地快速说明着。
　敏特孔多利雅变小了呢。应该是不能在没有本体的树木的地方移动的，是通过什麼方法办到的啊。
　从敏特孔多利雅的话中推测，船长在那之後，回到祭之岛了啊。但是，竟然答应再一次把船开到暗黑大陆啊。活着回去的话不好好答谢……可不行啊！　现在还在战鬥中。
　因为突然与他们的再会让我不禁松懈了，赶忙重整态势望向福奥尔。

「土屋先生。打倒他们就可以了吗」

「权藏、萨乌瓦。是相当强的对手。把福奥尔当成比兽人帝还要强的敌人比较好」

「了解」

「嗯，知道了」

「穿着白色燕尾服的是洛蒂。在迷宫认识的伙伴。洛蒂，这两个人和小不点、是之前说过的在祭之岛一起战鬥的伙伴」

　与两人虽然已经一年以上没见面了，但是是一同跨越困境的伙伴，就算没有详细说明也能够理解。
　权藏他们与洛蒂。一同度过的时间虽然差不多，但在亲密度上却有着相当的差距。
　虽然能够信用洛蒂，但心理还没彻底信赖她。因为，有利用她是福奥尔的妹妹的立场，所以我在避免着必要以上的接触。
　以防万一把线伸向两人，把现在的状况与福奥尔和三人所需要注意的地方利用『精神感应』传达。
　与洛蒂讲过在祭之岛發生的事了，所以刚才的说明应该就够了。
　对於把他人卷入感到有所顾虑，但如果是权藏与萨乌瓦的话就不需要介意了。我与他们――在樱变成圣树後发过誓。

　其一、就算遇到困难，造成其中有人失去性命也决不後悔。

　其一、尽全力让樱变回来。

　其一、对帮助家人不感到犹豫。

　就权藏所说的话「也就是、俺们三人间不需任何顾虑」的意思啊。
　在把握了两人的实力後，这仍是压倒性不利的状态，但这股安心感是为什麼呢。一同度过的充实日子、经歷，在背後支撑着我。

「啊，久仰各位的大名了。是权藏先生和萨乌瓦小姐呢。这边的小小人型是敏特孔多利雅小姐吧」

　洛蒂因为是和两人第一次见面，能很有礼貌的打招呼是很好，但现在还是这种情况所以希望还是等之後再这麼做。

「女人……向樱告密」

「向樱姐姐打小报告」

　快住手，萨乌瓦、权藏。
　紧张的气氛一瞬间缓和了下来。明明死神的镰刀都要挥下来了却还是笑了出来。
　不过，从那之后福奥尔就没有出手了。看起来，只是一直定眼观看这边的情况。

「怎麼。不攻过来吗？」

「啊啊，继续聊也没关係哦。身为男人这种气氛我还是看得懂得啊。还有很多想要说的话吧。不用在意这边」

　与刚才截然不同，感觉心情很好。
　这是吹了什麼风。有什麼内幕吗。

「土屋先生。那傢伙不是个挺好的人吗？」

「应该能对话啊」

　看见这样果然会这麼想啊。但内心是个狂人。认为正常的对话能成立是白费力的。以福奥尔为对象的话比起观察，直接问比较快。

「你想要幹嘛？」

「不，没……我是感到高兴啊。不仅能入手土屋君这个理想的素体，甚至还提供了新的素体。见识到把实验体给吹风的本领，相当的熟练技巧啊？　那边的女孩子的素质也很好。嗯嗯，实在是太棒了！　今天是多麼美好的日子啊！」

「前言撤回。是个怪人啊」

「同上」

　似乎也不用再说明了。

「那，就开始二回战吧。详细的在全部结束後再继续吧。两人也能帮忙吗？」

「呦，交给我吧」

「漂亮地完成」

「敏特孔多利雅也在喔！」

「我也会努力的」

「说的也是。三……四人都拜托了。赶快结束掉，再让我听听那之後岛上的事吧！」

　两人与那时相比手腕确实有提升。身上所缠着的气的质确实有所变化。
　敏特孔多利雅也有了契约，能够使用某种程度的精灵魔法。经由与我的主从契约，能感受到能力的一部份流入脑内。

「不、不好意思，可以在多一个人参加吗……」

　从打开的门中出现一个露出一点身体在窥看的人。
　熟悉的脸因为惊恐而动摇着。完全没注意到潜藏在这麼近的她的存在。

「修米米……你为什麼在这裡。妳的记忆不是」

　我强制的发动『同调』，应该让有关我的记忆强制的想不起来了啊。
　凭她的能力应该是无法抵抗。如此考虑的。

「土屋先生。正因为有修米米我们才能到这裡的。多亏了敏特土屋先生所在大致的方向是知道了，但详细的位置就无法得知。到达大陆後就在昆虫人的街道上进行情报收集，得知了蝗虫族曾受到土屋先生的帮助」

　乔布布和修米米的记忆虽然被伪造了，但其他的蝗虫族还是记得着。

「之後呢。详细的打听消息後，发现只有乔布布和修米米没有和哥哥的记忆，而且不知为何有着道具箱而感到可疑」

　萨乌瓦接在权藏後继续说着。
　就算权藏在後面露出不满的表情，但他的眼睛还是在看着福奥尔的方向。
　福奥尔那边没有任何动作。好像真的不会在对话中妨碍呢。

「然後，说了哥哥在祭之岛的活跃后，蝗虫族的人们也说了自己是怎麼被帮助的时侯……」

「只有我，全部都想起来了。兄长说总有一种很怀念的感觉，但是却无法想起来」

　从门往前走出来的修米米。身体前并在一起手忙碌地动着。怕会不会惹我生气的紧张着吧。

「明明都叫我逃跑了，还是回来了。非常抱歉……但是，我真的，想要成为你的力量！」

　她那双大眼睛裡映着我的身影。看着她那认真的眼神我吞下了否定的话语。

「我知道了。但是，死了就不要後悔哦。没有餘裕能够帮助你了」

「是！」

　想用冰冷的话语让她放弃的，但她脸颊放鬆露出放晴般的笑容。

「有听到吗权藏先生。这在日本语中叫什麼啊」

「叫做天然的小白脸，萨乌瓦酱。真是ーー」

　不要用邻居的太太的感觉来责备人。我不在的期间权藏与萨乌瓦的步调已经变得如此同步了啊。话说回来，萨乌瓦……你的角色是不是变了啊？

「哦，对了，忘了呢。土屋先生请你拿着这个」

　权藏拿出的是一根树枝。

「这是我的树的树枝所以不能用不见喔！」

　原来是这样。是因为把本体的树枝切了下来，才能带敏特孔多利雅一起过来啊。因为只有身体的一部份所以能力也受到限制，因此才这麼小啊。

「土屋离开以後，拼命地努力了，才能像这样移动了！　厲害吧？　厲害吧？」

「啊啊，很聪明啊」

　因为有敏特孔多利雅的成长，才能这样再次见面啊。

「那麼。来说正经的了……加上我们後你认为胜率有多高呢」

「大概有１０％左右吧」

「还真艰难啊。现在的这情况土屋先生得意的陷阱也没办法设置啊」

「毕竟哥哥的主力是陷阱啊」

　两人都好好地理解着。
　兽人帝也不是普通的对手，那是因为有设置陷阱所以才能赢的。那场战鬥如果是突然就进入战鬥状态的话毫无疑问一定是会输的。

「周围的那些傢伙也挺难对付的」

　明明只有福奥尔赢面就很低了，实验体的他们也还活着。
　首先必须先想办法对付他们。

「土屋先生。我会尽量拖住哥哥的。虽然没办法战胜，但拖延时间的话还是可以的」

　两人间有压倒性的实力差。但是，以妹妹为对手福奥尔应该不会全力以赴吧。刚才的怒火也已经消退了、现在的话应该是能够对话吧。

「那麼……拜托你了。这边会尽快的打倒他们，然後过去支援的」

「是。１０１、１０５、１０８就交给你们了。拜托你们了！」

　想到深深低下头的洛蒂的心理，感觉什麼也说不出来。
　她自己应该是不希望他们三人被杀死的吧，但也知道不是可以这麼任性的状况。所以、我只能这样说了吧。

「会尽全力的」

　不会说会杀死或打倒。实际上、也不觉得有那种餘裕。
　再加上，他们也有希望透过死亡来解脱的节操。活着好还是被杀死好，哪个判断比较正确，我无法决定。

「大家，请听我说。我们以那个白衣的男性……福奥尔以外的为对手。福奥尔交给妹妹的洛蒂。集合然後开始吧」

「哦呜」

「了解」

「是、是ー的」

「知道了」

「哥哥就拜托了」

　听完全员的回应，从正面望向福奥尔他们。
　实验体的他们保持着变成魔物的状态，不断地粗暴呼吸着，眼睛因为充满血丝而通红。理性还没回来啊。
　福奥尔坐着不知从哪拿出来的椅子，在读着书。真有餘裕啊。
　注意到我的视线而把书合上，把椅子和书放进在附近出现的黑暗漩涡内。和洛蒂使用的闇魔法是同一个种类吧。

「话已经说完了吗？　下次的对话机会是在重生後了哦」

「不用你操心。只要，把你打倒後就不用担心了」

「哦，还真有自信啊。在得知了我和实验体的实力後，那份坚强。真令人满意。真适合我所要制作的最强生物啊」

　不会有那样的未来的，不会让他到来的。
　与最强的伙伴会合了。此时不取胜更待何时。

「那麼，再次开战了！」

　与当初的预定一样，实验体的三人交给我们，距离远一点的福奥尔则交给洛蒂。
　就算洛蒂陷入苦战，因为逆境而陷入紧要关头我也不会去救她。虽然很过分，但对她的期待就只有拖延时间。
　必须在她被打倒之前，将这三人打倒、不然我们就没有明天了。

