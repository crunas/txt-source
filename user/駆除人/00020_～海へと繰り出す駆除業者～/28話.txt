“把你们的行李交给我吧!”

船厂的师傅、巴洛克骑上船我说。
造船所给我的是又旧又小的船,於是我打算是进行魔法改造，动力和防御。
因为领主的事情，我和贝尔斯、艾露时间很紧张。
但特露已经和我没有关係,所以不会被通缉,但还是有点担心了。
还好大家的眼睛是雪亮的,所以可以放心。

“不能参加你们的婚礼，真不好意思。”
“嗯，没关係，请您多加小心。”

特露握紧我做的通信袋说。
只要做好最後的準备就立即出發。
说是最後的準备，也只能把行李放在船上。
船只有游艇的大小，除了旧的和脏的以外，意外的结实，看起来没有什麼问题。

“最终还是和艾露分手了。”特露有些伤感。

“打算分手了吗?即使闘技会上夺冠我也不会坐那艘船的，直树大人，贝尔斯,以後也多多关照。”

艾露说着，拍打着我的肩膀。
出港的时候，不仅是特露和巴洛克，造船所的员工和街道的人也都挥手了。

“旅行是一期一会!珍惜相逢!”

特露笑了起来，挥了挥手向我们道别。（我是要成为海贼王的男人@( •̀∀•́ )）

船一直往东走。
风向是顺风。
海鸥相似的魔物和我们一起飞行前进。
过了一段时间，发现船的底部破了个洞，马上就开始进水了。
把墙板揭下来。
也许是因为担心领主的船会追上来，所以把产生水流的魔法团画在船的後面，加快速度，导致损坏。
一边修理船只，一边让船前进，确保床铺等杂务，我们三个人来一起完成。
厨房的清扫用了让目标变得乾净的魔法阵设置。
船裡厕所的地板掉了下去，所以用门做地板。
从外面看上去很奇怪，但是现在没办法了。
老实说，我觉得就算有幾根手也不够。
午饭是特露之前给我做的的三明治。

啊!

突然，船被什麼东西撞了一下。是海裡的魔物
章鱼吗？

我急着，虽然是加强了强化魔法阵的船，但还是有风险。
幸运的是，出现是海的魔物不是很强，因为是海怪，所以用了光魔法的魔法阵加上艾露的剑刺。
墨汁还有章鱼海怪的触手什麼的也可以採集,但不是很的美味。
船的损失幾乎没有。
接着袭来的是风。
原来船的帆很小,瞬间变得破破烂烂,舵的操纵不见效。
因为修理也很费事，所以就直接把魔力输入魔法阵，只靠流水前进。
等到风平静的时候,周围島影却消失了。
看到远处水平线。
我看了地图，不知道自己在哪裡。
依靠指南针,向东驶去，那边有岛屿。
大概一段时间,就会看到岛了。
只是这个岛周围有大量水母类的魔物、更多的章鱼海怪也袭击来了。
用中间裂开的窗户框架制作螺旋桨,加上风魔法的魔法阵保持动力。
之後系上绳索向大海裡扔去，窗框做的螺旋桨在海面上，固定好用来控制船的速度。
在魔法缓慢旋转的过程中，水中的水母被吸入窗框做的螺旋桨，被旋转的力量抛向空中。
在飞舞的地方，艾露则正挥舞剑斩开魔物。
似乎没有再生能力，逐渐消失在海的海裡。
不久,贝尔斯说看到了陆地,島影出现在东面的水平线上。
在靠近小岛的时候，魔物的个体变得更大了。
螺旋桨被堵了,所以将窗框做的螺旋桨回收。
在船的周围重新画上魔法阵,再次用水流在海裡前进。
回收的螺旋桨上刮起了龙卷风一样的风，便停止了魔力的输入，船渐渐的靠岸了

这座岛屿好像是孤岛，好像没有人。
岛就里都是密林的样子，能听到奇怪的妖物的叫声，还有像地震一样的魔物的脚步声。
