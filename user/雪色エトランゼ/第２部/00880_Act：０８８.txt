88
　祸之祠的最下层。
　通过巨大縦坑的底部、出现圆顶空间。
　只有优人他们的火把、无法看透这个空间的每一个角落。但是、从声音的回音的情况下、知道是相当的大。
　希兹娜小姐与秃头先生（忘记了！）、麻利地分火在预备的火把上、配置了光源，能看见整个圆顶。那个灵敏的动作、不愧是熟练的冒险者。和讶异地张大口的优人以及夏奈不同。
　……我也。
　火焰摇曳的光芒、照亮圆顶。
　天花板特别的高、火把的火焰还是照不到。果然是非常宽广的空间。并且在那个中央、有腐朽像祭坛的构造物。举行什麼的仪式吧。瓮和碟子、瓶等到处散乱着。
　然後、那个後面。
　稍微有点隆起来的地方、有一根岩石的柱子站在那裡。
　有二层楼的建筑物的高度吧。表面粗糙的岩层面裂缝、切割的薄薄。
「是这个」
　优人自言自语。
　确实、说起来不会看不见巨大的剑。但是、到底要怎麼说呢。
　其根源、相似的刺孔在柱子上、有无数个。
「嗯。到现在为止Braver拔去的封印的楔子的痕迹吧。优人君所言之事是正确的」
　马瑞斯坦博士、立刻开始仔细端详那个柱子。
「哇、爷爷、当心！」
　夏奈担心的跑到博士前面、伸出了手。
　但是、这样的东西是剑吗。
　我踮起脚、抬头看那柱子。。
　接近、轻轻地碰触一下。
　寂静冰冷的岩石的感触。
「优人君。那时、像女王型被解开那个时候一样地、向这个剑试着灌输银气」
　博士的话优人点了点头、膝盖弯曲、唰地飞起了。然後落在柱子的尖端。
「去」
　优人跪膝、手掌放在柱子上。
　银色的光辉、从优人的身体发出来。
　那淡淡的光辉、吞噬了火把的火、朦胧地开始照射地下空间。
　优人增强银色的光辉、摇晃、仿佛它本身活著似的把柱子开始包进去了。
「绮丽……」
　我为那个景象著迷。
　柱子它本身开始發出光芒。
　受到从优人流进去的银气、那个光辉渐渐增强。
　强烈的光。
　可是、没有刺痛的感觉。
　不如说是温暖的感觉、柔软温柔的光。是令人怀念、安心感环抱的光。
　眩银的光、把我们包进去。
「噢噢！」
　光之中、从哪裡听见马瑞斯坦博士的感叹的声音。
　伴随着一声巨响、复盖柱子表面的岩石剥落落下了。
　像个信号一样地、柱子喀哒喀哒开始振荡。
　空气开始震动。
「什麼、什麼！」
　希巴鲁兹大叫。
「哇、优人！希兹娜小姐！」
　夏奈大叫。
　光和振动越发强烈。
　在那耀眼的光芒下、我不由自主手遮脸。
　这时。　
　好像玻璃破了的尖锐的声音响起。
　在闪光中、在半睁的眼睛模模糊糊看见柱子成为光的粒子裂开飞了。
　银色的光芒在蔓延。
　接下来的瞬间、一口气朝向着优人收敛了。
「什麼……」
　瞬间、洞穴中回到黑暗和寂静。
　光芒眩光了眼睛、马上看不见黑暗中的状况、只听见从尖端着陆到地面的优人的脚步声。
　我取下手套，揉了揉眼睛。
　在刚刚有柱子的地方、只有优人伫立。
「优人、不要紧吗？」
　希兹娜小姐跑过去。
「优人君。封印的楔子……？」
　马瑞斯坦博士的脚被瓦砾困住、优人跑过去。
「优人……」
　眼神交汇。
　优人、对我强有力地点头了。
「封印的楔子。确实在这裡」
　优人这麼说、唰地举起手。
　优人的身体溢出银色光的粒子蔓延著、一瞬间就收敛在手中。
　优人的手裡、握著用银色的光形成的大剑。
　优人挥动了光之剑。
　發出切破天空的声音。
　确实那把剑、封印的楔子、是存在着。
「看来这是、以我的想象形式变化成的」
　优人看了自己手中的剑。
「对我来说、剑就是这种形式……」
　优人的光之剑、确实是平时优人使用着、由父亲给的Braver的大剑一样的形状。
「优人、帅！好酷！」
　夏奈發光的眼睛看著优人的手上。。
　马瑞斯坦博士敏捷的动著在优人四周打转、观察着光之剑。希兹娜小姐也很有兴趣地、一动不动地看着那个剑。
　光之剣到手、人类的仇敌魔兽打倒。
　确实……。
　我的手抱住胸下、慢慢的站到优人的隔壁。
「身体没事吗」
　我悄悄地仰视著优人。
「啊啊。反而充满力量、应该是这样的感觉」
　优人一手紧紧地握著、春风满面的俯视着我。
「请不要勉强。如果你又受伤了、我……」
　我垂下双眼。
　是我诱导优人去战鬥的……。
　但是、现在需要优秀人的力量。
　……我没有这个能力。
　但是、优人……。
　优人您们、希望平安。
　但是那、考虑到现在还有许多人在作战的的事、自私的愿望……。
　所以。
　所以、我、至少竭尽全力的笑著、抬头看着优人。
「所以……决定依赖著、优人」
　无论如何、想要守护大家。
　同样的、也希望优人平安。
　包含那样的愿望。
「哦、喔。交给我吧！」
　羞涩的优人点头的瞬间、乾燥的声音响著、优人的光之剑粉碎坍塌去了。
「哦、那、噢？」
　优人發出困惑的声音。
「……看来、剑的维持、需要使用者的集中力」
　平淡的声音、希兹娜小姐嘟囔着。
「啊、集中精神、优人」
　我用没关係的意思、叩叩的拍著优人的手臂。
「伽娜蒂酱……」
　不知为什麼夏奈半开着眼看我。
　什麼……？
　黑暗的洞穴中、马瑞斯坦博士与希兹娜小姐的笑声回响著。
　大家的笑声中、突然参杂拍手的声音。砰砰、砰砰的金属手套拍打声。
　与笑声绝对不合适的不谐和音。
　希兹娜小姐他们、一齐进入警戒姿态。我也把手放在腰上的剑警备、唰地环视周围。
　然後、注意到了。
　圆顶状的空间的深處。
　火把照明不到的黑暗地方、二个红色的光浮着。
　异常鲜红的光芒……。
　背发冷。
　心跳高涨。
　身体颤抖。
　两个红色的光芒、慢慢的靠近我们。刺耳的拍手不断。
「黒、骑士……！」
　优人恨恨的吐出。
「梵……」
　我的自言自语意外的大大地回响。
「异世界的来访者。祝贺得到楔子、毁灭我们的王牌。恭喜」
　黒骑士低沉的声音响著。
　没有感情的声音。可是现在、是很开心的口气。
　对自身有危害的凶器、我们到手了……。
「等很久了。这个时侯」
　梵停下掌声看我们。
　优人再次使出封印的楔子摆出姿态。我们也各自举起武器。
　但是黑骑士、用泰然自若的态度张开双臂。
「两支。这次、同时两支楔子被拔出了。我们服侍的人愈来愈狂妄、当Braver的领航巫女被符咒杀了时候、我也灰心了。还好、结果是圆满的」
　感觉浑身象电击了一样。
　我静静憋住气。
「伊、伊莉斯姊姊……」
　梵口中的傲慢的人、是黒骑士服侍的拉普雷男爵。假如、拉普雷男爵要诅咒过世的女性。
　那是、伊莉斯姊姊。
　领航人……。
　如马瑞斯坦博士所说。
　从异世界来的Braver、被这个世界的少女引导。
　那个、是伊莉斯姊姊。
　为了打倒魔兽而被这个世界招唤的优人、本来应该被利穆威尓侯爵家帮助、会遇到伊莉斯姊姊、又引导对魔兽的战鬥、应该这麼说吧。
「是因果」
　总是多话的梵、红色光寄宿的眼睛、专注的看我。
　被那个眼神注视著、吓一哆嗦身体震动。
「本来的领航人刚一消失、替代就準备了。喂、伽娜蒂君。你把Braver引导到这裡、我真的非常感激」
　黒骑士、突然以人类的身姿的时候的梵的声音向我笑了。
　我不由自主後退。
　惊愕的瞪大了双眼。
　用快要变得空白了的头、拼命地思考。
　马瑞斯坦博士的话。
　黒骑士梵的话。
　归根究柢、最初是父亲大人、请求我作为伊莉斯姊姊的代理。
　我、被这个世界作为伽娜蒂这个女性招唤来的、是为了成为代替被拉普雷的疯狂行为杀死的伊莉斯姊姊。代替著、引导Braver与魔兽的战鬥……。
　所以我、我在这裡……。
「黒骑士！向伽娜蒂灌输了什麼！」
　优人瞥了呆然的我一眼、拿著封印的楔子向黑骑士怒目而视。
「极好。因果的线、成为自己的愿望而拉过来」
　黑骑士回到了梵的声音、优人径自抬头望着天花板。
「安聂菲雅。这个因果操纵黑的力量吞食世界的话、能再一次我们生活在一起。在母亲的魔兽的肚子裡、连时间都超越了……」
　为了对抗魔兽Braver被叫来、与配置引导Braver的少女。
　那、是一个毁灭魔兽的系统。
　可是、看到欢喜的呼喊的黑骑士的身姿、不安膨胀。
　谁。
　做出这样的结构……？
　那个疑问、身体充满著难受的心情。

　讨厌的、预感、不会消失。
　我咬紧牙关、瞪著梵。
「黒骑士、那个口气、简直象是你们召集Braver来的」
　马瑞斯坦博士忍不住、出声。
　梵歪著头、用灿烂红色的眼睛看著马瑞斯坦博士。
「当然。操纵因果、劈开次元的力量、难道人能操纵吗」
　什麼。
「魔獣的存在和与其对抗的世界意识……」
　博士的自言自语、梵嗤鼻而笑。
「有这样的暧昧的东西存在吗？下等的人」
　那麼、是什麼呢。
　招唤我和优人到这个世界的东西！
　马瑞斯坦博士了解了什麼在惊愕皱眉头了。
　那个瞬间。
　偷偷的转到黑骑士背後的希巴鲁兹、跳过去挥著剑。
「喋喋不休的话太多！」
　希巴鲁兹的裂帛的吼叫在圆顶空间响遍。


　梵也不拿出红之剑、只用金属手套就一击弹飞了希巴鲁兹。金属猛撞的尖锐的声音在壁面反响。
　用手扶著地面、停住被吹跑姿势的希巴鲁兹、直接踢地再次突击。
「嘿、知道吗、黒骑士！反派角色都会多话、因为要盖棺论定了！」
　希巴鲁兹吼著。而且呼应似的、优人快速的斩击。
　可是梵、用手接住希巴鲁兹的剑、优人一击被从空中叫出的红之剑接住了。
　闯进的希巴鲁兹和优人对抗著黒骑士。
　刃紧紧地鸣响。
　梵、向逼近的优人靠近脸、低声的说。
「拔掉最後的楔子、谢谢」
　楔。
　与魔兽为天敌的剑。
　拥有强大银气的Braver世世代代从这个地方拔出、用来讨伐魔兽的封印的楔子。
　黒骑士、想要拔除……？　
　那个瞬间。
　大地开始震动。
「咭、地震！」
「伽、伽娜蒂酱！」
　希兹娜小姐和夏奈發出哀鸣声尖叫著。
　摇晃开始变得猛烈。
　从墙壁和天花板上淅淅沥沥地落下石头和砂子。
　大地呻吟著。
　脚下、简直像生物一样蠕动着、抖动！
「全体、躲避！」
　我叫喊的瞬间、封印的楔子扎住的地方崩的突然陷落了。滚滚霾刮上升。然後、那个周围开始凹陷、洞孔开始扩大了。
　世、世界本身摇摆不定着……！
　变得无法忍受、我不禁跪倒。
　那个瞬间、吹升起来的尘烟的对面、跳过陷落的洞的优人出现了。
　优人在我旁边着陆、从腰间轻轻地抱住我。我發出声音的空暇也没有、飞跃著离开洞穴了。
「大家都避难了！希巴鲁兹、博士！」
　被优人抱住、在地声的轰响中、我拼命的喊叫著。
「可恶、又是老头子！」
　希巴鲁兹的声音微微地听到了。
「是吗！」
　然後、马瑞斯坦博士呼喊声也听到了。
「封印的楔子、不是做为停止女王型活动的东西！是已经、封印住！在这片大地！怎麼回事！到现在为止的众多Braver是被黑骑士操纵著、一根一根的拔掉楔子！」
　那个最後的一支、现在、拔出来了……。
　在大地底下、什麼在蠢动。
（完）
