
即使闭上眼睛也能感受到那耀眼的阳光，慢慢睁开眼睛。
早晨的太阳从半打开的窗帘的缝隙照射进来。
可以听到鸟鸣声从外面传来。

“……早上了啊”

睁开眼睛拱起身体，深呼吸让意识清醒。

起床是件很难的事，不过，在作为勇者旅行的时候，会对自己施展起床後马上安定意识的术。
因为有过在睡觉时發生袭击的事情。

没有睡意，疲惫也完全消失。
好像很久没有这样了，没有梦的深深的睡眠。
被背叛之後都是一个响声就醒了的浅眠。

“久违了，啊……”

目光望向把被子弄得乱七八糟，在旁边的床上睡着的艾路菲。
在奈落的迷宫中，幾次在这傢伙的旁边睡着了，不过，那个时候由於警戒的原因一直睡眠不足。

但是，在这傢伙旁边的时候，都会普通的睡去了。

不如说――。


“我……”
“嗯……伊织”


艾路菲对在不知不觉中说出口的嘟哝有了反应。
睡眼惺忪的發出声音，咕噜地翻身。

口水从脸颊上流下。

“呼唤……厨师……。……不……还不坏。倒不如说。想要吃更多……”
“……”


什麼梦話啊。
哎呀……对这样无防备的傢伙继续警戒，应该是很难了吧。

“真是的……”

把因为睡相的关係歪掉的被子盖好，把房间的灯关掉
因为朝阳，外面完全亮起来了。
这样的話，就不会胆怯黑暗了吧。

走出房间，走在房子裡。
已经有佣人起床了，询问早餐怎麼办。

“起床以後一起吃”
“艾路菲小姐啊。好的，我明白了”
“对了。从那以後，卡伦怎麼样？”
“……不睡觉地继续工作。白天的話，去确认结界的样子”
“是这样啊……”


确实，卡伦，还有很多没做的事。
急需要的东西也很多吧。
但是，在那样的精神状态下没有睡觉，对身体有害。
一次也好，最好休息一下。

道别佣人，为了确认直接卡伦的情况，向她的房间走去。

“早上好，伊织先生”

突然和从另外的房间出来的让相遇了。
听说正在休养，不过，让穿着侍从的衣服。

“身体已经没问题了吗？”
“是的。原本就只是记忆有一点点暧昧而已。我没大碍。而且，在这辛苦的时候，没有休息的时间”


睡了一晚上，让就像平常一样工作着。
其他的佣人和约翰不同，好象已经休息好幾天了。

“听了您的話，伊织先生……给您添麻烦了，对不起。虽然说是被洗脑了，但是造成了负担”(お話は、伺いました、伊織さん……ご迷惑をお掛けして、申し訳ありませんでした。洗脳されていたとはいえ、私は大変なことを……)

“全部都是那个女人策划的。让没有必要谢罪”

阻止低下头的让。
元凶的奥莉维亚已经下地狱了。
这件事结束了。

卡伦也理解，所以，不追究让的行动。

“听说奥莉维亚……因为“事故”已经死亡。谢谢你。这下……卡修大人也……”
“……让先生”


卡修没能保护自己遭受攻击，奥莉维亚愤怒吗？。(不懂 ガッシュを守れなかった自分を攻めているのか、オリヴィアに憤っているのか)
让握紧拳头，礼貌的开口。

“……伊织先生要去卡伦大人那裡?”
“是的。我听说卡伦一直没有睡觉，想打一声招呼”
“是吗……。刚才我去打声招呼了，因为现在很忙所以不要打扰。帮我向其他人传达的，这样”(そうですか……。先ほど私も声をお掛けしたのですが、今は忙しいので放っておいて欲しいと仰られていました。他の方にもそう伝えて欲しい、とも)

“……那样啊”
“非常对不起。伊织先生的报酬再稍微等等”


礼貌地道歉後,让从那裡离开了。
现在跟谁都不想说話，是这样吧。

卡伦现在，光自己的事就竭尽全力了。
报酬是进入迷宫，提供情报。
不管怎麼样，现在应该是没办法说了。
说了午餐後去确认结界的样子，那个时候再问吧。

◆

那之後先回到房间，还在睡的艾路菲起来了。
和睡迷糊的艾路菲一起吃早餐，到书籍库收集信息。
调查的是今後我们将要挑战的“死沼迷宫”。

“这麼说来，伊织曾经一度突破了死沼迷宫”

坐在旁边的艾路菲，偶然嘟哝着。

“啊。帝国作为後盾，组成队伍”

如同死沼迷宫的名字，迷宫中有沼泽。
那并不是普通的沼泽。
身体接触到就会融化，沼泽放出瘴气只要沐浴到就会死亡的凶恶的毒沼。
毒沼洞窟的横向洞穴，死沼迷宫。

为了进去裡面，使用了防毒的魔法道具。
沼泽的瘴气有削弱魔力效果，与水魔将战鬥时有魔力不足陷入苦战的经验。

“哼……但是，只要看最近的书，迷宫的结构好像有很大的变化”
“……啊啊”


就像艾路菲说的那样，到现在为止得到的情报，迷宫的结构与之前不同。
出来的魔物数量不断增加，到处都是陷阱。
然後迷宫内部，洞窟中，像城堡一样的建造物好象持续增加着。

“欧路特吉亚吗，但现今的水魔将领改造了吧”(欧路特吉亚->把艾路菲封印的魔王)

“改造……吗。啊，艾路菲。迷宫是那麼简单就能改变的吗？”
“并不简单，但一般是可行的”


艾路菲说，迷宫内持有“魔王纹”的魔族——即魔王产生的一种的魔物。
亲生父母是魔王的話，消费大量魔力，就可以摆弄迷宫的结构。

同时，被魔王认可的人，迷宫内的魔物好象会服从。
如果利用它的話，即使不是魔王，也可以在迷宫中发动陷阱，改变构造。
不太有疑问，土魔将和火魔将之所以不会被其他的魔物攻击是因为这样的原因吧。

“如果改造的是水魔将的的話，是个智力高很麻烦的对手吧”
“……有如巴尔基鲁多那样智能发达的魔物，或者是魔族”(巴尔基鲁多->岩窟龙)


卡修留下来关於迷宫的手记中这样写着。
偶尔，在迷宫的入口，会有伤痕的魔物跑出来。
仿佛要逃避什麼样子，想要破坏结界闹腾着。

迷宫内的魔物互相争夺，或者被水魔将折磨了吗？
若是前者，那就是迷宫内的魔物很凶残。
如果是後者，水魔将是个性情相当粗暴的傢伙吧。

或者，魔物甚为享受。

不管是哪一方，这次的迷宫好像不能用普通的办法。

一脸险峻的脸，被敲了肩。

“放心吧。我们会打倒水魔将的。伊织总是那样，花点时间就可以了”(安心しろ。私がちょちょいと水魔将を倒してやる。伊織はいつも通り、時間を稼いでくれればいい)

麻利干练的表情，艾路菲这样说了。

“……啊。有了一定程度的魔力，应该能争取时间”(……ああ。ある程度魔力が戻ったから、それなりに時間を稼げるはずだ)

即使没有魔石，也能用一些魔术。
托翡翠太刀和红莲铠甲，身体能力和防御力也无可挑剔。
加上少许残余的魔石的力量，以魔将为对手也能战鬥。

“有什麼万一的話，伊织在我背後隐藏起来。因为手臂恢復了，也变得能接近战了。如果魔眼来不及，就用手臂战鬥”
“那个技能吗？……。确实威力很高，不过，消费的魔力大吗？”

“抑制威力的話，就能多次使用”


可靠极了。

在这样的情况下，关於死亡沼迷宫进行了预习，关於战鬥方法也进行了讨论。
关於毒，在温泉都市的时候，有两个人的準备。
在帝都，也购买了解毒药水，没有问题吧。

在这样做的时候，太阳正接近天顶。
就要中午了。

是卡伦去看结界的样子的时候了吧。

把书收好，拍掉尘土从房间出来的时候。
康康康康，从外面传来了敲钟的声音。

“好嘈杂啊”

钟声传来，房子裡有些吵闹。
听到来回走动的声音。

另外，窗外也有人呼喊的声音。
有谁在呼喊着什麼。
钟声的关係，在房子裡听不出来在说什麼。

“……好像不是什麼好事”
“在外边有什麼东西吧”
“艾路菲，去确认吧”


“啊”

出了房间，立刻找到了卡伦的身影。

和不是佣人，疑似是领民的男人在对話。

男人脸色都变了，相当慌张的样子。

“怎麼了？”

跑过去打声招呼。

“伊织先生和艾路菲小姐……。發生了，很严重的事情”

卡伦脸色很差。
相当不好的事情發生了吧。

“到底，怎麼……”

卡伦严肃的表情说道。

“——迷宫的结界，被“水魔将”打破了”

